﻿using UnityEngine;
using System.Collections;

// An singleton that helps the camera to transit between environments 
public class EventHandler : MonoBehaviour {

	public static EventHandler handler = null;  

	public GameObject player;

	public VREnvironment homeEnvironment;
	public VREnvironment videoEnvironment;

	//Awake is always called before any Start functions
	void Awake()
	{
		if (handler == null)
			handler = this;

		else if (handler != this)
			Destroy(gameObject);    

		DontDestroyOnLoad(gameObject);
	}

	void Start () {
		homeEnvironment.enabled = true;
		videoEnvironment.enabled = false;
	}

	void Update () {

	}

	public void triggerMovie(MovieTexture mov) {
		player.transform.position = videoEnvironment.transform.position;
		videoEnvironment.playMovie (mov);
	}

	public void backHome() {
		videoEnvironment.clean ();
		player.transform.position = homeEnvironment.transform.position;
	}
}
