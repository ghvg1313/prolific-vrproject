﻿using UnityEngine;
using System.Collections;

public class ExitObject : GazeObject {

	override protected void triggeredEvent() {
		EventHandler.handler.backHome ();
	}
}
