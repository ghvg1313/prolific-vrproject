﻿using UnityEngine;
using System.Collections;

public class GazeObject : MonoBehaviour {

	public MovieTexture movtexture;

	protected double gazeAtCount = 0;

	protected bool gazing = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (gazing) {
			print (gazeAtCount);
			gazeAtCount++;
		}

		if (gazeAtCount > 100) {
			GetComponent<BoxCollider> ().enabled = false;
			gazeAway ();
			triggeredEvent ();
		}
	}

	public void gazeAt() {
		gazing = true;
	}

	public void gazeAway() {
		gazing = false;
		gazeAtCount = 0;
		GetComponent<BoxCollider> ().enabled = true;
	}

	virtual protected void triggeredEvent() {
		EventHandler.handler.triggerMovie (movtexture);
	}
}
