﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof(AudioSource))]

public class VREnvironment : MonoBehaviour {

	private bool movieStarts = false;
	private MovieTexture mov;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void playMovie(MovieTexture movTexture) {
		if (!movieStarts) {
			mov = movTexture;
			mov.loop = true;
			movieStarts = true;	
			GetComponent<Renderer> ().material.mainTexture = mov;
			mov.Play ();
		}
	}

	public void clean() {
		mov.Stop ();
		movieStarts = false;
	}
		
}
