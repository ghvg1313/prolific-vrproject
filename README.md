# Project-Red-Pill
Take the red pill you shall create whatever you want

#Platform
Unity 5.3.2f1

Google Cardbord(iOS and Android)

#Before checking out
Make sure your Unity is the right version. Open the project using unmatched Unity version may cause the project to crash at certain point.

If you want to build mobile version rather than just checking in Editor mode, make sure you have XCode if you want to build on iOS,  Android Studio or other IDE for Android

#How to use
The main scene is under RedPill/ProlificOffice.scene, open the scene and hit play button on top to preview under editor mode.

To build, Select File/Build Settings, and then select the right platform for your device. Exporting and formatting files may be needed, wait until it's over. If build failed (Leaving an empty building folder after build finishes), check the console information.

For iOS, open the project file built by Unity, Remember to disable Bitcode under Build Settings section, and add `Security.framework` and `SystemConfiguration.framework` to link binary with libraries under Build phases section.

Unknown for Android right now, if you successfully build on Android, please update this section.

#Known Issue
Video preview is unstable in Editor Mode at this point, random crashes are excepted when previewing in Editor Mode. Make sure you save before you play in Editor Mode.

Project may crash on iOS, suspicion is that iOS forbids multiple video loading at the same time, make sure to minimize the number of videos be loaded at the same time.